# Small API

Very small stupid API. 

## Requirements

1. MySql or docker
1. PHP 7.2
1. All Laravel required dependencies (https://laravel.com/docs/5.7/installation#installation)
1. CURL PHP Extension
1. composer

## Preparations

1. Ensure MySQL is running and create required user and DB
or use docker compose if it is available (run `docker-compose up -d` in project root)
1. Create `.env` file from `.env.example`
1. Install dependencies and prepare DB
```bash
$ composer install
$ php artisan key:generate
$ php artisan migrate:install
$ php artisan migrate
```

## Testing

```bash
$ vendor/bin/phpunit
```

## Development server

```bash
$ php artisan serve
```

Visit http://127.0.0.1:8000 for API.

## API description

*Request format:* form data

*Response format:* JSON

### Create product

*Path:* /product/create

*Method:* POST

*Parameters:*

|Name|Format|Description|
|---|---|---|
|price|Integer|Product price in minor units. Minimal value: 1.|
|productType|String|Product type. Maximal length: 255.|
|color|String|Product color. Maximal length: 255.|
|size|String|Product size. Allowed values: S, M, L, XL, XXL, XXXL.|

**Note:** tuple <productType, color, size> must be unique.

*Response format:*

|Name|Format|Description|
|---|---|---|
|id|Integer|Product ID.|

*Response example:*

```json
{"id":  666}
```

### Create order draft and calculate total price

*Path:* /order/create

*Method:* POST

*Parameters:*

|Name|Format|Description|
|---|---|---|
|order|Array|Array containing products with amounts.|
|order.{idx}.product_id|Integer|Product ID to order, received from product create method.|
|order.{idx}.amount|Integer|Amount to order.|

**Note:** orders must have some minimal total price (1000 in minor units by default).

**Note:** requests count from the same country is limited bt time (by default: 10 requests per second).

*Request example:*

```
order[0][product_id]=666
order[0][amount]=1
order[1][product_id]=777
order[1][amount]=7
```

*Response format:*

|Name|Format|Description|
|---|---|---|
|id|Integer|Order draft ID.|
|total_price|Integer|Total price of all products in order in minor units.|

*Response example:*

```json
{"id":  888, "total_price":  1234}
```

### Get orders list

*Path:* /order/list/{productType}

*Method:* Get

*Parameters:*

|Name|Format|Description|
|---|---|---|
|productType|String|**Optional.** If set, only order drafts containing products of required type will be returned. Otherwise, all drafts will be returned.|

*Response format:* array of objects. Object properties are listed below.

|Name|Format|Description|
|---|---|---|
|id|Integer|Order draft ID.|
|country|String|User IP country ISO code in alpha-2 format. IF geoIP is unavailable, "US" is used as default value.|
|created_at|String|Draft creation datetime. Format: Y-m-d H:i:s.|
|updated_at|String|Draft modification datetime. Format: Y-m-d H:i:s.|
|products|Array|Array of products for current order.|
|products{idx}.id|Integer|Product ID.|
|products{idx}.price|Integer|Product price in minor units.|
|products{idx}.productType|String|Product type.|
|products{idx}.color|String|Product color.|
|products{idx}.size|Product size.|
|products{idx}.created_at|String|Product creation datetime. Format: Y-m-d H:i:s.|
|products{idx}.updated_at|String|Product modification datetime. Format: Y-m-d H:i:s.|
|pivot|Object|Order to product link information.|
|pivot.order_id|Integer|Order draft ID.|
|pivot.product_id|Integer|Product ID.|
|pivot.amount|Integer|Amount of ordered products.|

*Response example:*

```json
[
    {
        "id": 24,
        "country": "LV",
        "created_at": "2018-12-17 20:27:11",
        "updated_at": "2018-12-17 20:27:11",
        "products": [
            {
                "id": 6,
                "price": 1234,
                "productType": "T-shirt",
                "color": "red",
                "size": "XXXL",
                "created_at": "2018-12-17 20:27:04",
                "updated_at": "2018-12-17 20:27:04",
                "pivot": {
                    "order_id": 24,
                    "product_id": 6,
                    "amount": 999
                }
            }
        ]
    }
]
```

### Errors

**Validation error:**

*Response code:* 400
```json
{
  "errors": {
    "<field name 1>": ["<error>"],
    "<field name 2>": ["<error>"],
    /* etc. */ 
  }
}
```

**Maintenance:**

*Response code:* 503
```json
{"error": "Maintenance is in progress. Please, try later."}
```

**Unknown API method:**

*Response code:* 404 
```json
{"error": "Unknown API method."}
```

**Generic error:**

*Response code:* 500
```json
{"error": "Unknown error occurred. Sorry for inconvenience."}
```
