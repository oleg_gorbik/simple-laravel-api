<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'price' => $faker->randomNumber(),
        'productType' => $faker->unique()->name,
        'color' => $faker->colorName,
        'size' => $faker->randomElement(['S', 'M', 'L', 'XL', 'XXL', 'XXXL']),
    ];
});
