<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App
 *
 * @property string country
 */
class Order extends Model
{
    /**
     * Get all of the products for the order.
     */
    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot('amount');
    }
}
