<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    const DEFAULT_ORDER_MIN_TOTAL_PRICE = 1000;
    const DEFAULT_ORDER_LIMIT_PERIOD_S = 1;
    const DEFAULT_ORDER_LIMIT_REQUESTS_N = 10;

    private $orderMinTTotalPrice;
    private $orderLimitPeriodS;
    private $orderLimitRequestsN;

    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->orderMinTTotalPrice = env('ORDER_MIN_TOTAL_PRICE', self::DEFAULT_ORDER_MIN_TOTAL_PRICE);
        $this->orderLimitPeriodS = env('ORDER_LIMIT_PERIOD_S', self::DEFAULT_ORDER_LIMIT_PERIOD_S);
        $this->orderLimitRequestsN = env('ORDER_LIMIT_REQUESTS_N', self::DEFAULT_ORDER_LIMIT_REQUESTS_N);
    }


    /**
     * Create order draft in Database
     *
     * @param Request $request
     * @return Response|string Returns JSON with order ID and total price.
     */
    public function create(Request $request)
    {
        $validation = Validator::make(
            $request->post(),
            [
                'order' => 'required|array',
                'order.*.product_id' => 'required|integer|min:1',
                'order.*.amount' => 'required|integer|min:1',
            ]
        );
        if ($validation->fails()) {
            return Response::json(['errors' => $validation->errors()], 400);
        }

        $country = geoip()->getLocation($request->ip())['iso_code'] ?? 'US';
        $count = Order::where('created_at', '>=', date('Y-m-d H:i:s', time() - $this->orderLimitPeriodS))->where('country', '=', $country)->count();
        if ($count >= $this->orderLimitRequestsN) {
            return Response::json(['errors' => ['requests_count' => ['Too many requests']]], 400);
        }

        $errors = $this->prepareProductsData($request->post('order', []), $totalPrice, $productAmounts);
        if (!empty($errors)) {
            return Response::json(['errors' => $errors], 400);
        }

        $order = new Order();
        $order->country = $country;
        $order->save();
        foreach ($productAmounts as $id => $amount) {
            $order->products()->attach($id, ['amount' => $amount]);
        }

        return Response::json(['id' => $order->getKey(), 'total_price' => $totalPrice], 201);
    }

    /**
     * Create order draft in Database
     *
     * @param Request $request
     * @param $productType
     * @return Response|string
     */
    public function list(Request $request, $productType = null)
    {
        if ($productType === null || $productType === '') {
            $data = Order::all();
        } else {
            $data = Order::with('products')->whereHas('products', function($q) use ($productType) {
                $q->where('productType', $productType);
            })->get();
        }

        foreach ($data as $order) {
            $order->loadMissing('products');
        }
        return Response::json($data);
    }

    /**
     * Validate product existence and total price
     *
     * @param array $orderData
     * @param int $totalPrice
     * @param array $productAmounts
     * @return array List of errors
     */
    protected function prepareProductsData(array $orderData, &$totalPrice, &$productAmounts): array
    {
        $errors = [];
        $totalPrice = 0;
        $productAmounts = [];

        foreach ($orderData as $key => $value) {
            $product = Product::find($value['product_id']);
            if (!$product) {
                $errors["order.$key.product_id"] = ["No product with id '{$value['product_id']}' found."];
            } else {
                $totalPrice += $product->price * $value['amount'];
                $productAmounts[$value['product_id']] = $value['amount'];
            }
        }

        if ($totalPrice < $this->orderMinTTotalPrice) {
            $errors['total_price'] = ['Sum of all products must be at least ' . $this->orderMinTTotalPrice];
        }

        return $errors;
    }
}
