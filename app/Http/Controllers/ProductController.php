<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    const ALLOWED_SIZES = ['S', 'M', 'L', 'XL', 'XXL', 'XXXL'];

    /**
     * Create product record in Database
     *
     * @param Request $request
     * @return Response|string Returns JSON with product ID.
     */
    public function create(Request $request)
    {
        $validation = Validator::make(
            $request->post(),
            [
                'price' => 'required|integer|min:1',
                'productType' => "required|max:255|unique:products,productType,{$request->post('productType')},id,color,{$request->post('color')},size,{$request->post('size')}",
                'color' => 'required|max:255',
                'size' => ['required', Rule::in(self::ALLOWED_SIZES)],
            ],
            [
                'price.integer' => 'The :attribute must be an integrer in minor units.', // to prevent from precision issues
                'size.in' => 'The selected :attribute is invalid. Allowed values are: ' . implode(', ', self::ALLOWED_SIZES),
                'productType.unique' => '<productType, color, size> tuple must be unique.',
            ]
        );
        if ($validation->fails()){
            return Response::json(['errors' => $validation->errors()], 400);
        }

        $product = Product::create($request->post());
        return Response::json(['id' => $product->getKey()], 201);
    }
}
