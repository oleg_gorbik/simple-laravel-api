<?php

namespace Tests\Feature;

use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function testCreateSuccessWithDuplicationCheck()
    {
        $product = factory(Product::class)->make();

        $response = $this->post(
                '/product/create',
                [
                    'price' => $product->price,
                    'productType' => $product->productType,
                    'color' => $product->color,
                    'size' => $product->size,
                ]
            );

        $response->assertStatus(201)->assertJsonStructure(['id']);
        $this->assertDatabaseHas('products', [
            'price' => $product->price,
            'productType' => $product->productType,
            'color' => $product->color,
            'size' => $product->size,
        ]);

        $response = $this->post(
                '/product/create',
                [
                    'price' => $product->price,
                    'productType' => $product->productType,
                    'color' => $product->color,
                    'size' => $product->size,
                ]
            );
        $response->assertStatus(400)->assertJson(['errors' => ['productType' => ['<productType, color, size> tuple must be unique.']]]);
    }

    /**
     * @param $postData
     * @param $expectedErrors
     *
     * @dataProvider getErrorCases
     */
    public function testCreateValidationErrors($postData, $expectedErrors)
    {
        $product = factory(Product::class)->make();

        $response = $this->post('/product/create', $postData);

        $response->assertStatus(400)->assertJson(['errors' => $expectedErrors]);
        $this->assertDatabaseMissing('products', [
            'price' => $product->price,
            'productType' => $product->productType,
            'color' => $product->color,
            'size' => $product->size,
        ]);
    }

    public function getErrorCases()
    {
        return [
            [
                [],
                [
                    "price" => ["The price field is required."],
                    "productType" => ["The product type field is required."],
                    "color" => ["The color field is required."],
                    "size" => ["The size field is required."],
                ],
            ],
            [
                [
                    'price' => '12.34',
                    'productType' => str_pad('', 300, 'R'),
                    'color' => str_pad('', 300, 'R'),
                    'size' => 'QQ',
                ],
                [
                    "price" => ["The price must be an integrer in minor units."],
                    "productType" => ["The product type may not be greater than 255 characters."],
                    "color" => ["The color may not be greater than 255 characters."],
                    "size" => ["The selected size is invalid. Allowed values are: S, M, L, XL, XXL, XXXL"],
                ],
            ],
            [
                ['price' => 0, 'productType' => 'test', 'color' => 'green', 'size' => 'L'],
                ["price" => ["The price must be at least 1."]],
            ],
        ];
    }
}
