<?php

namespace Tests\Feature;

use App\Http\Controllers\OrderController;
use App\Order;
use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function testCreateSuccess()
    {
        $product1 = factory(Product::class)->make(['price' => 1000]);
        $product1->save();
        $product2 = factory(Product::class)->make(['price' => 2000]);
        $product2->save();

        $response = $this->post(
            '/order/create',
            [
                'order' => [
                    ['product_id' => $product1->getKey(), 'amount' => 2],
                    ['product_id' => $product2->getKey(), 'amount' => 3],
                ],
            ]
        );

        $response
            ->assertStatus(201)
            ->assertJsonStructure(['id', 'total_price'])
            ->assertJsonFragment(['total_price' => 8000]);


        $this->assertDatabaseHas('orders', [
            'country' => 'US',
        ]);

        $this->assertDatabaseHas('order_product', [
            'order_id' => 1,
            'product_id' => $product1->getKey(),
            'amount' => 2,
        ]);

        $this->assertDatabaseHas('order_product', [
            'order_id' => 1,
            'product_id' => $product2->getKey(),
            'amount' => 3,
        ]);
    }

    /**
     * @param $order
     * @param $expectedErrors
     *
     * @dataProvider getErrorCases
     */
    public function testCreateValidationError($order, $expectedErrors)
    {
        $response = $this->post('/order/create', $order);
        $response->assertStatus(400)->assertJson(['errors' => $expectedErrors]);
    }

    public function getErrorCases()
    {
        return [
            [[], ['order' => ['The order field is required.']]],
            [['order' => 'qwerty'], ['order' => ['The order must be an array.']]],
            [
                ['order' => [[]]],
                [
                    'order.0.product_id' => ['The order.0.product_id field is required.'],
                    'order.0.amount' => ['The order.0.amount field is required.'],
                ]
            ],
            [
                ['order' => [['product_id' => 'qwerty', 'amount' => 'qwerty']]],
                [
                    'order.0.product_id' => ["The order.0.product_id must be an integer."],
                    'order.0.amount' => ['The order.0.amount must be an integer.'],
                ]
            ],
            [
                ['order' => [['product_id' => 0, 'amount' => 0]]],
                [
                    'order.0.product_id' => ["The order.0.product_id must be at least 1."],
                    'order.0.amount' => ['The order.0.amount must be at least 1.'],
                ]
            ],
            [
                ['order' => [['product_id' => 666, 'amount' => 1]]],
                [
                    'order.0.product_id' => ["No product with id '666' found."],
                    'total_price' => ['Sum of all products must be at least ' . env('ORDER_MIN_TOTAL_PRICE', OrderController::DEFAULT_ORDER_MIN_TOTAL_PRICE)],
                ]
            ],
        ];
    }

    public function testCreateTooManyRequests()
    {
        factory(Order::class, env('ORDER_LIMIT_REQUESTS_N', OrderController::DEFAULT_ORDER_LIMIT_REQUESTS_N) * 2)->create(['country' => 'US']);

        $response = $this->post('/order/create', ['order' => [['product_id' => 666, 'amount' => 1]]]);
        $response->assertStatus(400)->assertJson(['errors' => ['requests_count' => ['Too many requests']]]);
    }

    public function testList()
    {
        $products = [];
        $orders = factory(Order::class, 2)
            ->create()
            ->each(function (Order $order) use (&$products) {
                $tmp = factory(Product::class)->create();
                $amount = $this->faker->randomNumber();
                $products[] = [
                    'product' => $tmp,
                    'amount' => $amount,
                ];

                $order->products()->attach($tmp->getKey(), ['amount' => $amount]);
            });

        $expectedOrders = [
            [
                'id' => $orders[0]->id,
                'country' => $orders[0]->country,
                'created_at' => $orders[0]->created_at->toDateTimeString(),
                'updated_at' => $orders[0]->updated_at->toDateTimeString(),
                'products' => [
                    [
                        'id' => $products[0]['product']->id,
                        'price' => $products[0]['product']->price,
                        'productType' => $products[0]['product']->productType,
                        'color' => $products[0]['product']->color,
                        'size' => $products[0]['product']->size,
                        'created_at' => $products[0]['product']->created_at->toDateTimeString(),
                        'updated_at' => $products[0]['product']->updated_at->toDateTimeString(),
                        'pivot' => [
                            'order_id' => $orders[0]->id,
                            'product_id' => $products[0]['product']->id,
                            'amount' => $products[0]['amount'],
                        ],
                    ],
                ],
            ],
            [
                'id' => $orders[1]->id,
                'country' => $orders[1]->country,
                'created_at' => $orders[1]->created_at->toDateTimeString(),
                'updated_at' => $orders[1]->updated_at->toDateTimeString(),
                'products' => [
                    [
                        'id' => $products[1]['product']->id,
                        'price' => $products[1]['product']->price,
                        'productType' => $products[1]['product']->productType,
                        'color' => $products[1]['product']->color,
                        'size' => $products[1]['product']->size,
                        'created_at' => $products[1]['product']->created_at->toDateTimeString(),
                        'updated_at' => $products[1]['product']->updated_at->toDateTimeString(),
                        'pivot' => [
                            'order_id' => $orders[1]->id,
                            'product_id' => $products[1]['product']->id,
                            'amount' => $products[1]['amount'],
                        ],
                    ],
                ],
            ],
        ];

        // get all orders
        $response = $this->get('/order/list');
        $response->assertStatus(200)->assertJson($expectedOrders);

        // get orders by product type
        $response = $this->get('/order/list/' . rawurlencode($products[0]['product']->productType));
        $response->assertStatus(200)->assertJson([$expectedOrders[0]]);

        $response = $this->get('/order/list/' . rawurlencode($products[1]['product']->productType));
        $response->assertStatus(200)->assertJson([$expectedOrders[1]]);

        // get orders for unknown product type
        $response = $this->get('/order/list/bla-bla-type');
        $response->assertStatus(200)->assertJson([]);
    }
}
